---
layout: markdown_page
title: "Working remotely"
---

Working remotely has a lot of advantages, but also a couple of challenges.
The goal of this section is to provide information and tips to maximize the
advantages and make you aware of the challenges and provide tips on how to deal
with them.

Probably the biggest advantage of working remotely and asynchronously is the
flexibility it provides. This makes it easy to **combine** work with your
personal life although it might be difficult to find the right **balance**.
This can be mitigated by either explicitly planning your time off or plan when
you do work. When you don't work it is recommended to make yourself unavailable
by turning off Slack and closing down your email client. Coworkers should
allow this to work by abiding by the [communication guidelines](https://about.gitlab.com/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus).

If you worked at an office before, now you lack a default group to go out to
lunch with. To look at it from a different perspective, now you can select who
you lunch with and who you do not lunch with. Haven't spoken to a good friend in
a while? Now you can have lunch together.

### Coffee Break Calls

Understanding that working remotely leads to mostly work-related conversations
with fellow GitLabbers, everyone is encouraged to dedicate **a few hours a week**
to having social calls with any teammate - get to know who you work with,
talk about everyday things and share a virtual cuppa' coffee. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. The Coffee Break calls are different to the
[Random Room](/handbook/communication/#random-room) video chat, they are meant for give you the option
to have 1x1 calls with specific teammates you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

### Tips on Ergonomic Working

The goal of [office ergonomics](http://ergo-plus.com/office-ergonomics-10-tips-to-help-you-avoid-fatigue/) is to design your office work station so that it fits you and allows for a comfortable working environment for maximum productivity and efficiency. Since we all work from home offices, GitLab wants to ensure that each team member has the [supplies](https://about.gitlab.com/handbook/spending-company-money/) and knowledge to create an ergonomic home office.

Below are some tips from the [Mayo Clinic](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/office-ergonomics/art-20046169) how on how arrange your work station.

1. Chair
* Choose a chair that supports your spinal curves. Adjust the height of your chair so that your feet rest flat on the floor or on a footrest and your thighs are parallel to the floor. Adjust armrests so your arms gently rest on them with your shoulders relaxed.

1. Keyboard and mouse
* Place your mouse within easy reach and on the same surface as your keyboard. While typing or using your mouse, keep your wrists straight, your upper arms close to your body, and your hands at or slightly below the level of your elbows. Use keyboard shortcuts to reduce extended mouse use. If possible, adjust the sensitivity of the mouse so you can use a light touch to operate it. Alternate the hand you use to operate the mouse by moving the mouse to the other side of your keyboard.
* Keep regularly used objects close to your body to minimize reaching. Stand up to reach anything that can't be comfortably reached while sitting.

1. Telephone
* If you frequently talk on the phone and type or write at the same time, place your phone on speaker or use a headset rather than cradling the phone between your head and neck.

1. Footrest
* If your chair is too high for you to rest your feet flat on the floor — or the height of your desk requires you to raise the height of your chair — use a footrest. If a footrest is not available, try using a small stool or a stack of sturdy books instead.

1. Desk
* Under the desk, make sure there's clearance for your knees, thighs and feet. If the desk is too low and can't be adjusted, place sturdy boards or blocks under the desk legs. If the desk is too high and can't be adjusted, raise your chair. Use a footrest to support your feet as needed. If your desk has a hard edge, pad the edge or use a wrist rest. Don't store items under your desk. GitLab recommends having an adjustable standing desk to avoid any issues.  

1. Monitor
* Place the monitor directly in front of you, about an arm's length away. The top of the screen should be at or slightly below eye level. The monitor should be directly behind your keyboard. If you wear bifocals, lower the monitor an additional 1 to 2 inches for more comfortable viewing. Place your monitor so that the brightest light source is to the side.
