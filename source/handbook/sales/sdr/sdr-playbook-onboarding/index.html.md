---
layout: markdown_page
title: "SDR Playbook Onboarding"
---

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

The playbook is ultimate success tool for an SDR.


Through this training course you will learn:
What the standards are for an SDR 
Buyer Personas 
SDR Framework
SDR Touch Pattern
SDR Qualification
SDR Handoff


It is important that you learn the playbook and how to use it in your job. This is a 1 week training course where you will learn these things.


Your manager will create an issue for each of the topics above to track the SDR’s progress by copying the content from the [raw markdown source](https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/source/handbook/sales/sdr/sdr-playbook-onboarding/index.html.md).


**Daily Planning**

Create a daily study plan to make sure you understand all the material included in the playbook and to complete all the tests/exercises.

## Day 1

#### Standards for an SDR: 

**Test:**

#### Buyer Responsive Messaging:

**Test:**

#### Buyer Personas: 

**Test:**

#### Value Propositions:

**Test:**

#### Use Cases:

**Test:**

## Day 2

#### SDR Framework

## Day 3

#### SDR Touch Pattern

## Day 4

#### SDR Qualification

## Day 5

#### SDR Handoff